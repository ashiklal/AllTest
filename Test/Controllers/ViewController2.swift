//
//  ViewController2.swift
//  weather app
//
//  Created by Ashik on 21/04/22.
//

import UIKit
import Alamofire
import Kingfisher

class ViewController2: UIViewController {
    
    var tempimg = ["hot","cool"]
    // Outlets
    @IBOutlet weak var txt1: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var topimg: UIImageView!
    
    
    
    // Actions
    @IBAction func searchBtnActn(_ sender: UIButton) {
        let manager = NetworkReachabilityManager(host: "www.apple.com")
        
        manager?.startListening { status in
            //            print("Network Status Changed: \(status)")
            if status == .reachable(Alamofire.NetworkReachabilityManager.NetworkReachabilityStatus.ConnectionType.ethernetOrWiFi){
                print("wifi")
            }
            else if status == .reachable(Alamofire.NetworkReachabilityManager.NetworkReachabilityStatus.ConnectionType.cellular){
                print("cellular")
            }
            else if status == .notReachable{
                print("No network avilable")
            }
        }
        if txt1.text?.count == 0{//textfield is empty
            label1.text = "No City found Please try again"
            label2.text = ""
            cityLabel.text = ""
            tempLabel.text = ""
            self.topimg.image = UIImage(named: "nrml")
        }
        else{//contains a city name
            webservices(locationName: txt1.text ?? "")//passing city along with api call function
            //            apicall(locationName: txt1.text ?? "")
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        let mygif = UIImage.gifImageWithName("giphy", ft: 1000)
        //        topimg.image = mygif
        
    }
    // functions
    func webservices(locationName :String){// Calling api with location from textfield
        let query = "/weather?q=\(locationName)&appid=\(APPID)&units=metric"
        let urldata = URL(string: baseURL+query)
        var request = URLRequest(url: urldata!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            do{
                
                let response = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                let userdata = response as! [String:Any]
                dict = userdata
                print(userdata["name"] as? String)
                DispatchQueue.main.async {
                    self.cityLabel.text = userdata["name"] as? String
                    if self.cityLabel.text?.count == nil{//city name in response = nil or invalid
                        self.label1.text = "No City found Please try again"
                        self.label2.text = ""
                        self.tempLabel.text = ""
                        self.topimg.image = UIImage(named: "nrml")
                    }
                    else{//having a valid city name
                        self.label1.text = "Ctiy name"
                        self.label2.text = "Temperature in Celsius"
                        var f = (((userdata["main"] as? [String:Any])?["temp"] as? Double) as? Double)
                        self.tempLabel.text = String(f ?? 0 )+" °C"
                        if (f! < 20){
                            print("Cool days")
                            self.topimg.image = UIImage(named: "c")
                        }
                        else if (f! > 20){
                            print("hot days")
                            self.topimg.image = UIImage(named: "s")
                        }
                        else {
                            self.topimg.image = UIImage(named: "nrml")
                        }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            
        }
        task.resume()
    }
    
    func apicall(locationName :String){
        // https://api.openweathermap.org/data/2.5/weather?appid=78f35552b600de8e5bbc6c9903223605&q=kollam
        let param = ["q" :locationName] as! [String:Any]
        AF.request("https://api.openweathermap.org/data/2.5/weather?appid=78f35552b600de8e5bbc6c9903223605&", method: .get, parameters: param).responseJSON { response in
            print(response)
            
            
        }
        
        
        
    }
    
}
